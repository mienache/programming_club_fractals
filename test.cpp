#include <iostream>
#include <complex>

#ifndef INCLUDED_IMAGE
#include "image.h"
#endif

#ifndef INCLUDED_RGBCOLOUR
#include "rgbcolour.h"
#endif

RGBColour mapStepToPixel(int step) 
{
    const int greyScale = step * 8;
    return RGBColour(greyScale, greyScale, greyScale);
}

RGBColour getMandelbrotValue(double x, double y)
{
    std::complex<double> startPoint(x, y);
    const double LIMIT = 2.0;
    std::complex<double> z(0, 0);
    for (int step = 1; step < 32; ++step) {
        if (z.real() > LIMIT) {
            return mapStepToPixel(step);
        }

        z = z * z + startPoint;
    }

    return mapStepToPixel(32);
}

void drawMandelbrotFractal(Image& image, int width, int height)
{
    double widthStep = (double) 3 / width;
    double heightStep = (double) 3 / height;
    
    int cx = 0;
    for (double x = -2; x < 1; x += widthStep) {
        int cy = 0;
        for (double y = -1.5; y < 1.5; y += heightStep) {
            RGBColour pixel = getMandelbrotValue(x, y);
            image.setPixel(cx, cy, pixel);
            ++cy;
        }
        ++cx;
    }
    
}

int main()
{
    const int MAX_COL = 255;
    const int NUM_ROWS = 500;
    const int NUM_COLS = 500;
    Image image(MAX_COL, NUM_COLS, NUM_ROWS);

    drawMandelbrotFractal(image, NUM_COLS, NUM_ROWS);

    std::cout << "Write the image to a file\n";
    image.writeToFile("mandelbrot2");

    return 0;
}
