#include "image.h"

#include <fstream>

Image::Image(int maxCol, int initWidth, int initHeight)
: MAX_COL(maxCol)
, width(initWidth)
, height(initHeight)
, pixels(height, std::vector <RGBColour> (width))
{
}

RGBColour Image::getPixel(int row, int col)
{
    return pixels[row][col];
}

void Image::writeToFile(const char *fileName)
{
    std::ofstream output(fileName);

    output << "P3\n";
    output << width << "\n";
    output << height << "\n";
    output << MAX_COL << "\n";
    for (int row = 0; row < height; ++row) {
        for (int col = 0; col < width; ++col) {
            output << pixels[row][col].getRedValue() << " ";
            output << pixels[row][col].getGreenValue() << " ";
            output << pixels[row][col].getBlueValue();
            if(col < width - 1) {
                output << " ";
            }
        }
        output << "\n";
    }
    
    output.close();
}

void Image::setPixel(int row, int col, const RGBColour& newPixel)
{
    pixels[row][col] = newPixel;
}
