#include "rgbcolour.h"

RGBColour::RGBColour()
: redValue(0)
, greenValue(0)
, blueValue(0)
{
}

RGBColour::RGBColour(int initRedVal, int initGreenVal, int initBlueVal)
: redValue(initRedVal)
, greenValue(initGreenVal)
, blueValue(initBlueVal)
{
}


int RGBColour::getRedValue()
{
    return redValue;
}

int RGBColour::getGreenValue()
{
    return greenValue;
}

int RGBColour::getBlueValue()
{
    return blueValue;
}

void RGBColour::setRedValue(int newRedVal)
{
    redValue = newRedVal;
}

void RGBColour::setGreenValue(int newGreenValue)
{
    greenValue = newGreenValue;
}

void RGBColour::setBlueValue(int newBlueValue)
{
    blueValue = newBlueValue;
}
