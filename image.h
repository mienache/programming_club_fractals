#define INCLUDED_IMAGE

#include <vector>

#ifndef INCLUDED_RGBCOLOUR
#include "rgbcolour.h"
#endif

class Image
{
  private:
    const int MAX_COL;
    int width;
    int height;
    std::vector < std::vector <RGBColour> > pixels;

  public:
    Image(int maxCol, int initWidth, int initHeight);

    RGBColour getPixel(int row, int col);
    void writeToFile(const char *fileName);
    
    void setPixel(int row, int col, const RGBColour& newPixel);
};
