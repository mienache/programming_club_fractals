#define INCLUDED_RGBCOLOUR

class RGBColour 
{
  private:
    int redValue;
    int greenValue;
    int blueValue;

  public:
    RGBColour();
    RGBColour(int initRedVal, int initGreenVal, int initBlueVal);
    
    int getRedValue();
    int getGreenValue();
    int getBlueValue();

    void setRedValue(int newRedVal);
    void setGreenValue(int newGreenVal);
    void setBlueValue(int newBlueVal);
};
